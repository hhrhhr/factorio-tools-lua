@echo off

set CC=e:\devel\tcc\tcc.exe
set V=0

set R=%~dp0%
set DIST=%R%..\factorio-tools-lua_dist
set ROOT_SRC=%R%src

set DIST_BIN=%DIST%\bin
set DIST_INC=%DIST%\include
set DIST_LIB=%DIST%\lib
set DIST_SHARE=%DIST%\share
set DIST_LUA_LIB=%DIST_LIB%\lua\5.3
set DIST_LUA_SHARE=%DIST_SHARE%\lua\5.3

set ERR=0

mkdir %DIST_BIN% %DIST_INC% %DIST_LIB% %DIST_SHARE% %DIST_LUA_LIB% %DIST_LUA_SHARE% >nul 2>&1

if . == %1. (
    call :make
) else (
    call :%1
)
cd /d %R%
echo all done
pause
goto :eof

:make
call :build_lua
call :build_zlib
call :build_libzip
call :build_libpng
call :build_libgd
call :build_luafilesystem
call :build_lua_zlib
call :build_lua_zip
call :build_lua_gd
call :build_md5
call :build_serpent
call :build_luasocket
call :build_binaryheap
call :build_timerwheel
call :build_copas
call :build_xavante
call :build_json
call :build_webcalc
call :build

goto :eof


:build_lua
cd /d %ROOT_SRC%\lua

rem core
set SRC=lapi.c lcode.c lctype.c ldebug.c ldo.c ldump.c lfunc.c lgc.c llex.c lmem.c lobject.c lopcodes.c lparser.c lstate.c lstring.c ltable.c ltm.c lundump.c lvm.c lzio.c
rem lib
set SRC=%SRC% lauxlib.c lbaselib.c lbitlib.c lcorolib.c ldblib.c liolib.c lmathlib.c loslib.c lstrlib.c ltablib.c lutf8lib.c loadlib.c linit.c
set H=lua.h luaconf.h lualib.h lauxlib.h lua.hpp
set CFLAGS=-DLUA_BUILD_AS_DLL
set LDFLAGS=-L%DIST_LIB% -llua53

echo build lua...
call :cmd %CC% -shared -o %DIST_BIN%\lua53.dll %CFLAGS% %SRC%
call :cmd move %DIST_BIN%\lua53.def %DIST_LIB% >nul 2>&1
call :cmd %CC% -o %DIST_BIN%\lua53.exe %CFLAGS% lua.c %LDFLAGS%
call :install . %DIST_INC% %H%
if %ERR% gtr 0 echo FAIL
goto :eof


:build_zlib
cd /d %ROOT_SRC%\zlib

set SRC=adler32.c compress.c crc32.c deflate.c gzclose.c gzlib.c gzread.c gzwrite.c infback.c inffast.c inflate.c inftrees.c trees.c uncompr.c zutil.c
set H=zlib.h zconf.h
set CFLAGS=-DZLIB_DLL

echo build zlib...
call :cmd %CC% -shared -o %DIST_BIN%\zlib1.dll %CFLAGS% %SRC%
call :install . %DIST_INC% %H%
call :cmd move %DIST_BIN%\zlib1.def %DIST_LIB% >nul 2>&1
if %ERR% gtr 0 echo FAIL
goto :eof


:build_libzip
cd /d %ROOT_SRC%\libzip

rem !!! zip_source_filep.c
rem !!! need comment create_temp_output() which use _zip_mkstempm() (no in Win build)

set SRC=zip_add.c zip_add_dir.c zip_add_entry.c zip_algorithm_deflate.c zip_buffer.c zip_close.c zip_delete.c zip_dir_add.c zip_dirent.c zip_discard.c zip_entry.c zip_err_str.c zip_error.c zip_error_clear.c zip_error_get.c zip_error_get_sys_type.c zip_error_strerror.c zip_error_to_str.c zip_extra_field.c zip_extra_field_api.c zip_fclose.c zip_fdopen.c zip_file_add.c zip_file_error_clear.c zip_file_error_get.c zip_file_get_comment.c zip_file_get_external_attributes.c zip_file_get_offset.c zip_file_rename.c zip_file_replace.c zip_file_set_comment.c zip_file_set_encryption.c zip_file_set_external_attributes.c zip_file_set_mtime.c zip_file_strerror.c zip_filerange_crc.c zip_fopen.c zip_fopen_encrypted.c zip_fopen_index.c zip_fopen_index_encrypted.c zip_fread.c zip_fseek.c zip_ftell.c zip_get_archive_comment.c zip_get_archive_flag.c zip_get_encryption_implementation.c zip_get_file_comment.c zip_get_name.c zip_get_num_entries.c zip_get_num_files.c zip_hash.c zip_io_util.c zip_libzip_version.c zip_memdup.c zip_name_locate.c zip_new.c zip_open.c zip_progress.c zip_rename.c zip_replace.c zip_set_archive_comment.c zip_set_archive_flag.c zip_set_default_password.c zip_set_file_comment.c zip_set_file_compression.c zip_set_name.c zip_source_accept_empty.c zip_source_begin_write.c zip_source_begin_write_cloning.c zip_source_buffer.c zip_source_call.c zip_source_close.c zip_source_commit_write.c zip_source_compress.c zip_source_crc.c zip_source_error.c zip_source_filep.c zip_source_free.c zip_source_function.c zip_source_get_compression_flags.c zip_source_is_deleted.c zip_source_layered.c zip_source_open.c zip_source_pkware.c zip_source_read.c zip_source_remove.c zip_source_rollback_write.c zip_source_seek.c zip_source_seek_write.c zip_source_stat.c zip_source_supports.c zip_source_tell.c zip_source_tell_write.c zip_source_window.c zip_source_write.c zip_source_zip.c zip_source_zip_new.c zip_stat.c zip_stat_index.c zip_stat_init.c zip_strerror.c zip_string.c zip_unchange.c zip_unchange_all.c zip_unchange_archive.c zip_unchange_data.c zip_utf-8.c
rem windows
set SRC=%SRC% zip_source_win32handle.c zip_source_win32utf8.c zip_source_win32w.c zip_source_win32a.c

set CFLAGS=-DHAVE_CONFIG_H -I%DIST_INC% -I.
set LDFLAGS=-ladvapi32 -L%DIST_LIB% -lzlib1

echo build libzip...
call :cmd %CC% -shared -o %DIST_BIN%\libzip.dll %CFLAGS% %SRC% %LDFLAGS%
call :install . %DIST_INC% zip.h zipconf.h
call :cmd move %DIST_BIN%\libzip.def %DIST_LIB% >nul 2>&1
if %ERR% gtr 0 echo FAIL
goto :eof


:build_libpng
cd /d %ROOT_SRC%\libpng

set SRC=png.c pngerror.c pngget.c pngmem.c pngpread.c pngread.c pngrio.c pngrtran.c pngrutil.c pngset.c pngtrans.c pngwio.c pngwrite.c pngwtran.c pngwutil.c
set H=png.h pngconf.h pnglibconf.h
set CFLAGS=-DPNG_BUILD_DLL -I%DIST_INC%
set LDFLAGS=-L%DIST_LIB% -lzlib1

echo build libpng...
call :cmd %CC% -shared -o %DIST_BIN%\libpng16.dll %CFLAGS% %SRC% %LDFLAGS%
call :install . %DIST_INC% %H%
call :move %DIST_BIN%\libpng16.def %DIST_LIB% >nul 2>&1
if %ERR% gtr 0 echo FAIL
goto :eof


:build_libgd
cd /d %ROOT_SRC%\libgd

set SRC=gd.c gd_bmp.c gd_color.c gd_color_map.c gd_color_match.c gd_crop.c gd_filename.c gd_filter.c gd_gd.c gd_gd2.c gd_gif_in.c gd_gif_out.c gd_interpolation.c gd_io.c gd_io_dp.c gd_io_file.c gd_io_ss.c gd_jpeg.c gd_matrix.c gd_nnquant.c gd_png.c gd_rotate.c gd_security.c gd_ss.c gd_tga.c gd_tiff.c gd_topal.c gd_transform.c gd_version.c gd_wbmp.c gd_webp.c gd_xbm.c gdcache.c gdfontg.c gdfontl.c gdfontmb.c gdfonts.c gdfontt.c gdft.c gdfx.c gdhelpers.c gdkanji.c gdtables.c gdxpm.c wbmp.c
set SRC=%src% floorf.c
set H=gd.h gdfx.h gd_io.h gdcache.h gdfontg.h gdfontl.h gdfontmb.h gdfonts.h gdfontt.h entities.h gd_color_map.h gd_errors.h gdpp.h
set CFLAGS=-DHAVE_CONFIG_H -I%DIST_INC%
set LDFLAGS=-L%DIST_LIB% -lpng16

echo build libgd...
call :cmd %CC% -shared -o %DIST_BIN%\libgd.dll %CFLAGS% %SRC% %LDFLAGS%
call :install . %DIST_INC% %H%
call :cmd move %DIST_BIN%\libgd.def %DIST_LIB% >nul 2>&1
if %ERR% gtr 0 echo FAIL
goto :eof


:build_luafilesystem
cd /d %ROOT_SRC%\luafilesystem

set CFLAGS=-I%DIST_INC%
set LDFLAGS=-L%DIST_LIB% -llua53

echo build luafilesystem...
call :cmd %CC% -shared -o %DIST_LUA_LIB%\lfs.dll %CFLAGS% lfs.c %LDFLAGS%
if %ERR% gtr 0 echo FAIL
goto :eof


:build_lua_zlib
cd /d %ROOT_SRC%\lua-zlib

set CFLAGS=-DLUA_LIB -DLUA_BUILD_AS_DLL -I%DIST_INC%
set LDFLAGS=-L%DIST_LIB% -llua53 -lzlib1

echo build lua-zlib
call :cmd %CC% -shared -o %DIST_LUA_LIB%\zlib.dll %CFLAGS% lua_zlib.c %LDFLAGS%
if %ERR% gtr 0 echo FAIL
goto :eof


:build_lua_zip
cd /d %ROOT_SRC%\lua-zip

set CFLAGS=-DLUA_LIB -DLUA_BUILD_AS_DLL -I%DIST_INC%
set LDFLAGS=-L%DIST_LIB% -llua53 -lzip

mkdir %DIST_LUA_LIB%\brimworks 2>nul

echo build lua-zip
call :cmd %CC% -shared -o %DIST_LUA_LIB%\brimworks\zip.dll %CFLAGS% lua_zip.c %LDFLAGS%
if %ERR% gtr 0 echo FAIL
goto :eof


:build_lua_gd
cd /d %R%/src/lua-gd

set CFLAGS=-DVERSION=\"2.0.33r3\" -DGD_PNG -DLUA_LIB -DLUA_BUILD_AS_DLL -I%DIST_INC%
set LDFLAGS=-L%DIST_LIB% -llua53 -lgd

echo build lua-gd
call :cmd %CC% -shared -o %DIST_LUA_LIB%\gd.dll %CFLAGS% luagd.c %LDFLAGS%
if %ERR% gtr 0 echo FAIL
goto :eof


:build_md5
cd /d %ROOT_SRC%\md5

set CFLAGS=-DLUA_LIB -DLUA_BUILD_AS_DLL -I%DIST_INC%
set LDFLAGS=-L%DIST_LIB% -llua53

echo build lua-md5
call :cmd %CC% -shared -o %DIST_LUA_LIB%\md5.dll %CFLAGS% md5.c md5lib.c %LDFLAGS%

call :install . %DIST_LUA_SHARE% md5.lua
if %ERR% gtr 0 echo FAIL
goto :eof


:build_serpent
cd /d %ROOT_SRC%\serpent

echo install serpent
call :install . %DIST_LUA_SHARE% serpent.lua
if %ERR% gtr 0 echo FAIL
goto :eof


:build_luasocket
cd /d %ROOT_SRC%\luasocket

rem socket
set SRC=luasocket.c timeout.c buffer.c io.c auxiliar.c options.c inet.c except.c select.c tcp.c udp.c compat.c
set SRC=%SRC% wsocket.c gai_strerror.c
set CFLAGS=-I%DIST_INC%
set LDFLAGS=-lws2_32 -L%DIST_LIB% -llua53
mkdir %DIST_LUA_LIB%\socket 2>nul
mkdir %DIST_LUA_LIB%\mime 2>nul

echo build luasocket...
call :cmd %CC% -shared -o %DIST_LUA_LIB%\socket\core.dll %CFLAGS% %SRC% %LDFLAGS%

rem mime
set SRC=mime.c compat.c

echo build mime...
call :cmd %CC% -shared -o %DIST_LUA_LIB%\mime\core.dll %CFLAGS% %SRC% %LDFLAGS%

set SOCK_T=ltn12.lua socket.lua mime.lua
set SOCK_S=http.lua url.lua tp.lua ftp.lua headers.lua smtp.lua
mkdir %DIST_LUA_SHARE%\socket 2>nul
call :install . %DIST_LUA_SHARE% %SOCK_T%
call :install . %DIST_LUA_SHARE%\socket %SOCK_S%
if %ERR% gtr 0 echo FAIL
goto :eof


:build_binaryheap
cd /d %ROOT_SRC%\binaryheap

echo install binaryheap
call :install . %DIST_LUA_SHARE% binaryheap.lua
if %ERR% gtr 0 echo FAIL
goto :eof


:build_timerwheel
cd /d %ROOT_SRC%\timerwheel

rem !!! need to comment 'require("coxpcall")'

echo install timerwheel
call :install . %DIST_LUA_SHARE% timerwheel.lua
if %ERR% gtr 0 echo FAIL
goto :eof


:build_copas
cd /d %ROOT_SRC%\copas

mkdir %DIST_LUA_SHARE%\copas 2>nul

echo install copas
call :install . %DIST_LUA_SHARE% copas.lua
call :install .\copas %DIST_LUA_SHARE%\copas *.lua
if %ERR% gtr 0 echo FAIL
goto :eof


:build_xavante
cd /d %ROOT_SRC%\xavante

set H=cgiluahandler.lua encoding.lua filehandler.lua httpd.lua indexhandler.lua mime.lua patternhandler.lua redirecthandler.lua ruleshandler.lua urlhandler.lua vhostshandler.lua
mkdir %DIST_LUA_SHARE%\xavante 2>nul

echo install xavante
call :install . %DIST_LUA_SHARE% xavante.lua
call :install . %DIST_LUA_SHARE%\xavante %H%
if %ERR% gtr 0 echo FAIL
goto :eof


:build_json
cd /d %ROOT_SRC%\json

echo install JSON
call :install . %DIST_LUA_SHARE% JSON.lua
goto eof


:build_webcalc
cd /d %ROOT_SRC%\factorio-web-calc

echo install web-calc
call :install . %DIST_SHARE%\web-calc /MIR
if %ERR% gtr 0 echo FAIL
goto :eof


:build
cd /d %ROOT_SRC%

mkdir %DIST_LUA_SHARE%\factorio 2>nul

echo install main scripts
call :install .\factorio %DIST_LUA_SHARE%\factorio cfgparser.lua defines.lua globals.lua loader.lua processdata.lua settingloader.lua ZipModLoader.lua
call :install . %DIST_LUA_SHARE% factorio_calc.lua factorio_dump.lua factorio_options.lua
call :install . %DIST% factorio_dump.cmd factorio_calc.cmd
goto eof


:cmd
if %ERR% gtr 0 goto eof
set CMD=%*
if 1. == %V%. echo =^> %CMD%
%CMD%
set ERR=%ERRORLEVEL%
goto :eof


:install
if %ERR% gtr 0 goto eof
set CMD=%*
if 1. == %V%. echo =^> robocopy %CMD%
robocopy %CMD% /nfl /njh /njs /ndl /nc /ns >nul
rem set ERR=%ERRORLEVEL%
goto :eof


:move
if %ERR% gtr 0 goto eof
set CMD=%*
if 1. == %V%. echo =^> move %CMD%
move %CMD%
set ERR=%ERRORLEVEL%
goto :eof


:eof
