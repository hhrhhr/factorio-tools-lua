# Warning!

Repository is frozen. Actual version - https://bitbucket.org/hhrhhr/factorio-lab-tools

## Build on Windows

Edit mass_make.cmd and set:

* `CC` = path to TCC compiler
* `DIST` = path to install dir

## Usage

Build (or download ready package from [Downloads](https://hhrhhr@bitbucket.org/hhrhhr/factorio-tools-lua/downloads/) )

Edit *share\lua\5.3\factorio_options.lua* and set:

* `game_path` = path to Factorio dir
* `mod_path` = path to mod dir
* `calcDir` = path to *KirkMcDonald/factorio-web-calc* (defaut is *share/web-calc*)

Run *factorio_calc.cmd* and open http://127.0.0.1:8080 in browser.

You can run *factorio_dump.cmd* for convert game date to _**prefix**-**version**.json_ and _sprite-sheet-**hash**.png_ saved to _**calcDir**/data_ and _**calcDir**/images_.
