local opt = {
    calcDir = "share/web-calc",
    game_path = [[f:\SteamLibrary\steamapps\common\Factorio]],
    mod_path = [[d:\Users\adm\AppData\Roaming\Factorio\mods]],
    prefix = "vanilla",
    verbose = false,
    open_browser = false,
}
return opt
