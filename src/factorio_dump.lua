local dump = {}

data = nil

local MD5 = require("md5")
local Loader = require("factorio.loader")
local Process = require("factorio.processdata")
local gd = require("gd")
local zlib = require("zlib")

local iconSZ = 32

local function LoadData(isPretty, opt)
    local locales = Loader.load_data(opt.game_path, opt.mod_path)

    local res = Process.process_data(data.raw, locales, opt.verbose)

    local icon_count = #res.icons
    local width = res.width
    local height = math.floor(icon_count / width)
    if icon_count % width > 0 then
        height = height + 1
    end

    local imageWidth = width * iconSZ
    local imageHeight = height * iconSZ

    local canvas = gd.createTrueColor(imageWidth, imageHeight)
    canvas:fill(0, 0, gd.TRANSPARENT)
    canvas:saveAlpha(true)

    for i = 0, icon_count-1 do
        io.write(".")
        local ico = res.icons[i+1]
        local source = ico.source
        local path = ico.path

        local icon
        if "file" == source then
            icon = gd.createFromPng(path)
        elseif "zip" == source then
            assert(false, "zip not implemented")
        else
            assert(false, "empty source")
        end

        local dstX = (i % width) * iconSZ
        local dstY = math.floor(i / width) * iconSZ
        local srcX = 64 == icon:sizeY() and 64 or 0

        canvas:copy(icon, dstX, dstY, srcX, 0, iconSZ, iconSZ)
    end
    io.write("\n")

    canvas = canvas:pngStr()
    local hexDigest = MD5.sumhexa(canvas)
    res.data.sprites.hash = hexDigest

--[[
    local w = io.open("data2.raw", "w+b")
    w:write(serpent.block(res.data, {comment = false, numformat = "%.16g", indent = " "}))
    w:close()
    w = io.open("data_normal.raw", "w+b")
    w:write(serpent.block(res.normal, {comment = false, numformat = "%.16g", indent = " "}))
    w:close()
    w = io.open("data_expensive.raw", "w+b")
    w:write(serpent.block(res.expensive, {comment = false, numformat = "%.16g", indent = " "}))
    w:close()
]]

    local eof, bytes_in, bytes_out

    res.data.recipes = res.normal
    local normalJSON = isPretty and JSON:encode_pretty(res.data) or JSON:encode(res.data)
    normalJSON, eof, bytes_in, bytes_out = zlib.deflate()(normalJSON, "finish")

    res.data.recipes = res.expensive
    local expensiveJSON = isPretty and JSON:encode_pretty(res.data) or JSON:encode(res.data)
    expensiveJSON, eof, bytes_in, bytes_out = zlib.deflate()(expensiveJSON, "finish")

    local override_fmt = [[
"use strict"
var OVERRIDE = [%q, %d, %d];
]]
    local override = override_fmt:format(res.version, imageWidth, imageHeight)

    return {
        Normal = normalJSON,
        Expensive = expensiveJSON,
        SpriteSheet = canvas,
        SpriteHash = hexDigest or "hexDigest",
        Version = res.version or "version",
        Override = override,
        Width = imageWidth,
        Height = imageHeight,
    }
end

local function write_file(fn, data)
    local w = io.open(fn, "w+b")
    if w then
        w:write(data)
    else
        print(("fail... %s"):format(fn))
    end
end

function dump.init(needWrite, opt)
    local options = opt or require("factorio_options")
    
    local d = LoadData(needWrite, options)

    if needWrite then
        local ver = d.Version
        local w = d.Width
        local h = d.Height

        local spritePath = ("%s/images/sprite-sheet-%s.png"):format(options.calcDir, d.SpriteHash)
        local normalDataPath = ("%s/data/%s-%s.json"):format(options.calcDir, options.prefix, ver)
        local expensiveDataPath = ("%s/data/%s-%s-expensive.json"):format(options.calcDir, options.prefix, ver)

        write_file(spritePath, d.SpriteSheet)
        write_file(normalDataPath, d.Normal)
        write_file(expensiveDataPath, d.Expensive)

        print(("files writed:\n  %s\n  %s\n  %s\n"):format(
                spritePath, normalDataPath, expensiveDataPath))

        print(([[
add these lines to %s/settings.js:

var MODIFICATIONS = {
// ...,
    "%s": new Modification("%s %s", "%s-%s.json", false, [%d, %d]),
    "%sx": new Modification("%s %s - Expensive", "%s-%s-expensive.json", false, [%d, %d])
// ...
}]]):format(options.calcDir,
                ver, options.prefix, ver, options.prefix, ver, w, h,
                ver, options.prefix, ver, options.prefix, ver, w, h
            ))

    else
        return d
    end
end

-- check for run from factorio_calc
if factorio_calc then
    return dump
else
    dump.init(true)
end
