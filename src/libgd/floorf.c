// TCC32 have a trouble with floorf
#ifndef _FLOORF_DEFINED
#define _FLOORF_DEFINED
#if defined(__TINYC__) && defined(__i386)
#include <math.h>
float floorf(x) { return floor((float)x); }
#endif
#endif //_FLOORF_DEFINED
