#ifndef _CONFIG_H
#define _CONFIG_H
#define BGDWIN32
#define ENABLE_GD_FORMATS ON
#define HAVE_DIRENT_H
#define HAVE_INTTYPES_H
#define HAVE_LIBM
#define HAVE_LIBPNG
#define HAVE_LIBZ
#define HAVE_STDINT_H
#define HAVE_STRINGS_H
#define HAVE_SYS_STAT_H
#define HAVE_SYS_TYPES_H
#define HAVE_UNISTD_H
#define PACKAGE
#define PACKAGE_NAME
#endif //_CONFIG_H
